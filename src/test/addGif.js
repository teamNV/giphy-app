import { addGifs } from "../functions/searchingFn.js";

/**
 * search gifs
 */
export const addGif = () => {
  const input = document.getElementById('searchPad')
  addGifs(input.value);
}