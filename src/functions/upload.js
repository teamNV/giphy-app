import { clearContent } from '../constantsAndFunctions/clear.js';
import giphyKey from '../constantsAndFunctions/giphyKey.js';
import { getGifById } from '../requests/apiServer.js';
import { singleGifView } from '../views/singleGifView.js'

// here we will keep the ID of uploaded content; 
let uploadedGifs = [];
/**
 * when click Submit, add the image and then return the id of the uploaded image
 */
// attach event may be change and put it in the index.js, when clicked, for now it's good
export const attachEvents = () => {
  document.querySelector('#submit-form-btn').addEventListener('click', async (e) => {
    e.preventDefault();
    const form = document.getElementById('uploadForm');
    const formData = new FormData(form);
    formData.append('api_key', giphyKey);
    if (formData.get('file').type !== 'image/gif') return alert('Expected gif image!');
    clearContent();
    document.getElementById('body').innerHTML = `
    <div>
      <img class=loadPic src="https://redom.bg/img/loading-cat.gif">
    </div>`;
    const response = await fetch('https://upload.giphy.com/v1/gifs',
      {
        method: 'POST',
        body: formData,
      });
    const json = await response.json();
    const id = json.data.id

    // the id's 
    uploadedGifs.push(id);
    
    const gifToView = await getGifById(id);
    document.getElementById('body').innerHTML = singleGifView(gifToView.data);
  })
};
/**
 * here are the id's we will use in our search request, so we can make them visible in the Uploaded gifs nav; 
 */
export const getUploaded = () => [...uploadedGifs];
