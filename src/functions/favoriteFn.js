import { clearPad } from "../constantsAndFunctions/clear.js";
import { id } from "../constantsAndFunctions/qAndqs.js";
import { getGifsByIds } from "../requests/apiServer.js"
import { favoriteView } from "../views/favoriteView.js";


/**
 * 
 * @param {ids} ids the request from the server
 */
export async function favoriteFn(ids) {
   const response = await getGifsByIds(ids);
   clearPad();
   const visGifs = id('body')
   visGifs.innerHTML = favoriteView(response);

}
