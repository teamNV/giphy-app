
import { clearContent } from "../constantsAndFunctions/clear.js";
import { searchingRes} from "../views/searchingView.js";
import { giphySearch } from "../requests/apiServer.js";
import { id } from "../constantsAndFunctions/qAndqs.js";

/**
 * 
 * @param {keyword} value to search with;
 * 
 */

export async function addGifs(value) {
  const result = await giphySearch(value);
  clearContent();
  const visGifs = id('body')
  visGifs.innerHTML = searchingRes(result);
};