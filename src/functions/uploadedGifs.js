import { clearContent } from "../constantsAndFunctions/clear.js";
import { id } from "../constantsAndFunctions/qAndqs.js";
import { getGifsByIds} from "../requests/apiServer.js";
import { uploadedView } from "../views/uploadedGifsView.js";
/**
 * 
 * @param {ids} ids gets the ids from the server
 */
export async function uploadedGif(ids) {
  const response = await getGifsByIds(ids);
  clearContent();
  const visGifs = id('body');
  visGifs.innerHTML = uploadedView(response);
}
