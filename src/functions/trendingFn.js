
import { trendingView } from "../views/trendingView.js";
import { giphySearchTrending } from "../requests/apiServer.js";
import { id } from "../constantsAndFunctions/qAndqs.js";
/**
 * parsing the gifs in the body
 */
export async function getTrending() {
  const result = await giphySearchTrending();
  const visGifs = id('body')
  visGifs.innerHTML += trendingView(result);
};

