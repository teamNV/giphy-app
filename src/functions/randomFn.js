
import { clearPad } from "../constantsAndFunctions/clear.js";
import { id } from "../constantsAndFunctions/qAndqs.js";
import { getRandomGif } from "../requests/apiServer.js";
import { randomGifView } from "../views/randomView.js";
/**
 * returns random gif
 */
export async function getRandomFinal() {
  const result = await getRandomGif();
  clearPad();
  const visGifs = id('body')
  visGifs.innerHTML = randomGifView(result);
  
};
