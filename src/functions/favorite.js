import { loadHomeFavorites } from "../services/homeFav.js";

let fav = [];
/**
 * 
 * @param {id} id 
 * @returns added fav id to favorite * local *
 */
export const addToFav = (id) => {

  if (fav.find(currId => currId === id)) {
    return;
  }
  const str = localStorage.getItem("fav");
  fav = (!str) ? [] : JSON.parse(str);
  fav.push(id);
  localStorage.setItem("fav", JSON.stringify(fav));
};

/**
 * 
 * @param {id} id 
 * remove id from favorite * local *
 */
export const removeFromFav = (id) => {
  fav = fav.filter(currId => currId !== id);
  localStorage.setItem('fav', JSON.stringify(fav));
};

/**
 * 
 * @returns the id's * you should use this to make them visible in the Favorite nav.
 */
export const getFav = () => [...fav];

/** 
 * remove and add by click;
 */
export const toggleFavoriteStatus = (id) => {
  const favorites = getFav();
  if (location.hash !== '#favorites') {
    if (favorites.includes(id)) {
      removeFromFav(id);
    } else {
      addToFav(id);
    }
  } else if (location.hash === '#favorites'){
    if (favorites.includes(id)) {
      removeFromFav(id);
    } else {
      addToFav(id);
    }
  }

};