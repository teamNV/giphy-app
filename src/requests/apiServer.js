import giphyKey from "../constantsAndFunctions/giphyKey.js";
import { offset } from "../index.js";
/**
 * 
 * @param {keyword} keyword the keyword to search with
 * @returns response from the api
 */
export async function giphySearch(keyword) {
  const response = await fetch(`http://api.giphy.com/v1/gifs/search?q=${keyword}&api_key=${giphyKey}&limit=24&offset=${offset}`);
  return await response.json();
};

/**
 * 
 * @returns response from the api
 */
export async function giphySearchTrending() {
  const response = await fetch(`http://api.giphy.com/v1/gifs/trending?&api_key=${giphyKey}&limit=20&offset=${offset}`);
  return await response.json();
};

/**
 * 
 * @returns response from the api
 */
export const getRandomGif = async () => {
  const response = await fetch(`https://api.giphy.com/v1/gifs/random?api_key=${giphyKey}`);

  return await response.json();
}

/**
 * 
 * @param {id} id 
 * @returns response from the api with the id
 */
export const getGifById = async (id) => {
  const response = await fetch('https://api.giphy.com/v1/gifs/' + id + '?api_key=' + giphyKey);

  return await response.json();
}

/**
 * 
 * @param {ids} ids 
 * @returns response from the api with the ids
 */
export const getGifsByIds = async (ids) => {
  const response = await fetch('https://api.giphy.com/v1/gifs?api_key=' + giphyKey + '&ids=' + ids.join(','));

  return await response.json();
}

