import { singleGifView } from "./singleGifView.js";
/**
 * 
 * @param {data} res return the gifs and add it to the body
 * @returns 
 */
export const favoriteView = (res) => {
  const gifs = res.data.map(element => {
    return singleGifView(element);
  });
  return gifs.join('');
};