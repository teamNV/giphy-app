
import { singleGifView } from "./singleGifView.js";
/**
 * 
 * @param {data} res 
 * @returns map for every gif by singleGifView; 
 */
export const searchingRes = (res) => {
  const gifs = res.data.map(element => {
    return singleGifView(element);
  });
  return `<div class="infoSearch"> <span>The best gifs from your search:</span></div>${gifs.join('')}`;
}

