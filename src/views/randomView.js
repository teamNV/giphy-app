
/**
 * 
 * @param {data} res 
 * @returns random gif
 */
export const randomGifView = (res) => {
 return  `
<div class='pic'>
<img class="loadedGifs" src=${res.data.images.original.url} />
<div class="information">
  <div class="infoUs">
    <p class="userinfo">
      username: ${res.data.user && res.data.user.username ? res.data.user.username : 'no username!'}
    </p>
    <p class="date">
      date:${res.data.import_datetime}
    </p>
  </div>
  <div class="fav">
  <a href="#" class="favButton" data-gifId="${res.data.id}">♡</a>
  </div>
</div>
</div>
`
};