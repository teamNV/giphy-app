import { getFav } from "../functions/favorite.js"

/**
 * 
 * @param {data} element 
 * @returns the gif data and pic
 */
 export const singleGifView = (element) => `
 <div class='pic'>
 <img class="loadedGifs" src=${element.images.original.url} />
 <div class="information">
   <div class="infoUs">
     <p class="userinfo">
       username: ${element.user && element.user.username ? element.user.username : 'no username!'}
     </p>
     <p class="date">
       date:${element.import_datetime}
     </p>
   </div>
   <div class="fav">
   ${renderFavoriteStatus(element.id)}
   </div>
 </div>
 </div>
 `;

/**
 * 
 * @param {data} element 
 * @returns gif view
 */

 export const renderFavoriteStatus = (gifId) => {
  const favorites = getFav();

  return favorites.includes(gifId)
    ? `<span class="favButton" data-gifId="${gifId}">❤</span>`
    : `<span class="favButton" data-gifId="${gifId}">♡</span>`;
};
