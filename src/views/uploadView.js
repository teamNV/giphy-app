import { attachEvents } from "../functions/upload.js";
/**
 * upload view;
 */
export const upload = () => {
  const doc = document.getElementById('body')
  doc.innerHTML = ` 
  <form id="uploadForm">
    <input type="file" name="file" id="upl">
    <button type="submit" id="submit-form-btn">Submit</button>
  </form>`;
  attachEvents()
}