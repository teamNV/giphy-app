
import { singleGifView } from "./singleGifView.js";
/**
 * 
 * @param {data} res 
 * @returns the gif with the information for it
 */
export const trendingView = (res) => {
  const gifs = res.data.map(element => {
    return singleGifView(element);
  });
  return gifs.join('') ;
}