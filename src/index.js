import { addGifs } from './functions/searchingFn.js'
import { getTrending } from "./functions/trendingFn.js";
import { clearContent, clearPad } from "./constantsAndFunctions/clear.js";
import { toggleFavoriteStatus } from "./functions/favorite.js";
import { upload } from './views/uploadView.js';
import { loadHomeFavorites } from './services/homeFav.js';
import { loadHomeUpload } from './services/homeUploaded.js';
import { addGif } from './test/addGif.js';
export let offset = 20;

document.addEventListener('DOMContentLoaded', () => {

  // add global listener
  document.addEventListener('click', event => {

    // lens click
    if (event.target.id === 'search') {
      addGif();
    }

    // trending
    if (event.target.id === 'trending') {
      clearPad();
      getTrending();
    }

    // fav 
    if (event.target.id === 'favorites') {
      loadHomeFavorites()
    }

    // fav Button
    if (event.target.classList.contains('favButton')) {
      toggleFavoriteStatus(event.target.getAttribute('data-gifId'));
    }

    // clears the page;
    if (event.target.id === 'name') {
      clearPad();
    }

    if (event.target.id === 'upload') {
      upload();
    }

    if (event.target.id === 'uploaded') {
      loadHomeUpload()
    };

  });

  document.addEventListener('scroll', () => {
    if (window.scrollY + window.innerHeight >= document.documentElement.scrollHeight) {
      if (location.hash === '#trending') {
        offset += 20;
        getTrending();
      };
    }
  })
  // search event
  document.getElementById('searchPad').addEventListener('keypress', event => {
    if (event.key === 'Enter') {
      addGifs(event.target.value);
      location.hash = '#search';
    };
  });
});


