import { id } from "../constantsAndFunctions/qAndqs.js";
import { getUploaded } from "../functions/upload.js";
import { uploadedGif } from "../functions/uploadedGifs.js";
/**
 * if there are not uploaded, it return text, otherwise returns the uploaded gifs
 */
export const loadHomeUpload = async () => {
  let uploaded = getUploaded();

  if (uploaded.length === 0) {
    const home = id('body');
    home.innerHTML = //not ready
    `
    <div class="uploadGifs">
    <p class="textArea">No gifs uploaded</p>
    </div>
    `
  } else {
   await uploadedGif(uploaded);
  }
}