import { favoriteFn } from "../functions/favoriteFn.js";
import { getFav } from "../functions/favorite.js";
import { getRandomFinal } from "../functions/randomFn.js";
/**
 * if there is no favorit gif, it returns random gif, otherwise it returns the favorites 
 */
export const loadHomeFavorites = async () => {
  let favorite = getFav();

  if (favorite.length === 0) {
    getRandomFinal();
    
  } else {
    
   await favoriteFn(favorite);
  }
};