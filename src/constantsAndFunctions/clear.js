
/**
 * @function clears the content from previous search 
 */

import { id } from "./qAndqs.js";

export const clearContent = () => {
  const myContent = id('body')
  myContent.innerHTML = "";
};

/**
 * @function clears the content and the search tab
 */
export const clearPad = () => {
  const myContent = id('body')
  myContent.innerHTML = "";
  const myPad = id('searchPad');
  myPad.value = "";
}


