/**
 * 
 * @param {element} selector take element by class
 * @returns 
 */
export const q = selector => document.querySelector(selector);

/**
 * 
 * @param {element} selector every element with X class
 * @returns 
 */
export const qs = selector => Array.from(document.querySelectorAll(selector));

/**
 * 
 * @param {element} selector take element by id
 * @returns 
 */
export const id = selector => document.getElementById(selector);

